package uk.co.paulbenn.jsonpointers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import uk.co.paulbenn.jsonpointer.gen.ParserException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class Rfc6901ComplianceVerifierTest {

    private ComplianceVerifier complianceVerifier;

    @BeforeEach
    void setUp() {
        complianceVerifier = new Rfc6901ComplianceVerifier(new Rfc6901Parser());
    }

    @Test
    void emptyString() {
        String jsonPointer = "";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void singleSlash() {
        String jsonPointer = "/";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void multipleSlash() {
        String jsonPointer = "/////";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void firstRangeStart() {
        String jsonPointer = "/\u0000";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void firstRangeEnd() {
        String jsonPointer = "/\u002E";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void secondRangeStart() {
        String jsonPointer = "/\u0030";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void secondRangeEnd() {
        String jsonPointer = "/\u007D";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void thirdRangeStart() {
        String jsonPointer = "/\u007F";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void lastSupportedCharacter() {
        // Georgian alphabet end - aParse2 limitation
        String jsonPointer = "/\u10FF";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void multipleRanges() {
        String jsonPointer = "/abc/012/.*}";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void combinedEmptyAndNonEmptyReferences() {
        String jsonPointer = "///abc///";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void escapedCharacters() {
        String jsonPointer = "/~0/~1/~0/~1";
        assertEquals(jsonPointer, complianceVerifier.verify(jsonPointer));
    }

    @Test
    void nullArgument() {
        assertThrows(IllegalArgumentException.class, () -> complianceVerifier.verify(null));
    }

    @Test
    void nonSlashStart() {
        assertThrows(ParserException.class, () -> complianceVerifier.verify("#/"));
    }

    @Test
    void unsupportedCharacter() {
        // Hangul Jamo alphabet start
        assertThrows(ParserException.class, () -> complianceVerifier.verify("/\u1100"));
    }
}
