package uk.co.paulbenn.jsonpointers;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import uk.co.paulbenn.jsonpointer.gen.Rule_json_pointer;

import static org.junit.jupiter.api.Assertions.assertTrue;

class Rfc6901ParserTest {

    private Rfc6901Parser parser;

    @BeforeEach
    void setUp() {
        parser = new Rfc6901Parser();
    }

    @Test
    void parse() {
        assertTrue(parser.parse("/abc") instanceof Rule_json_pointer);
    }
}
