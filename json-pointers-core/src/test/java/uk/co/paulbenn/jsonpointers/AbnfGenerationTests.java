package uk.co.paulbenn.jsonpointers;

import com.github.javaparser.ParseResult;
import com.github.javaparser.utils.SourceRoot;
import org.junit.jupiter.api.Test;

import java.nio.file.Paths;

import static org.junit.jupiter.api.Assertions.assertTrue;

class AbnfGenerationTests {

    @Test
    void generatedCodeCompiles() {
        assertTrue(
            new SourceRoot(Paths.get("src/gen/java"))
                .tryToParseParallelized()
                .stream()
                .allMatch(ParseResult::isSuccessful)
        );
    }
}
