package uk.co.paulbenn.jsonpointers;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class EscapeSequenceTest {

    @Test
    void unescapeOk() {
        assertEquals("~/~/", EscapeSequence.unescape("~0~1~0~1"));
    }

    @Test
    void unescapeDoesNotAffectSoleTilde() {
        assertEquals("123~abc", EscapeSequence.unescape("123~abc"));
    }
}
