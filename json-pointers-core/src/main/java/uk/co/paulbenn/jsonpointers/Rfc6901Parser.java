package uk.co.paulbenn.jsonpointers;

import uk.co.paulbenn.jsonpointer.gen.Rule;
import uk.co.paulbenn.jsonpointer.gen.Rule_json_pointer;

public class Rfc6901Parser implements Parser<Rule> {

    @Override
    public Rule parse(String jsonPointer) {
        return uk.co.paulbenn.jsonpointer.gen.Parser.parse(Rule_json_pointer.RULE_ID, jsonPointer);
    }
}
