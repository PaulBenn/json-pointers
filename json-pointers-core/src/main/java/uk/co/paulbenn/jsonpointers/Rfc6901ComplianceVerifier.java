package uk.co.paulbenn.jsonpointers;

public class Rfc6901ComplianceVerifier implements ComplianceVerifier {

    private final Rfc6901Parser rfc6901Parser;

    public Rfc6901ComplianceVerifier(Rfc6901Parser rfc6901Parser) {
        this.rfc6901Parser = rfc6901Parser;
    }

    @Override
    public String verify(String jsonPointer) {
        rfc6901Parser.parse(jsonPointer);
        return jsonPointer;
    }
}
