package uk.co.paulbenn.jsonpointers;

public final class StaticComplianceVerifier {

    private static ComplianceVerifier defaultComplianceVerifier;

    private StaticComplianceVerifier() {

    }

    public static String verifyCompliance(String jsonPointer) {
        return verifyCompliance(jsonPointer, getDefaultComplianceVerifier());
    }

    public static String verifyCompliance(String jsonPointer, ComplianceVerifier complianceVerifier) {
        return complianceVerifier.verify(jsonPointer);
    }

    private static ComplianceVerifier getDefaultComplianceVerifier() {
        if (defaultComplianceVerifier == null) {
            defaultComplianceVerifier = new Rfc6901ComplianceVerifier(new Rfc6901Parser());
        }
        return defaultComplianceVerifier;
    }
}
