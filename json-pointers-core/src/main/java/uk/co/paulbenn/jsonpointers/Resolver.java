package uk.co.paulbenn.jsonpointers;

import java.util.Arrays;

public abstract class Resolver<T> {

    public static final String KEY_SEPARATOR = "/";

    public T resolve(String jsonPointer, T json) {
        return resolveValidPointer(StaticComplianceVerifier.verifyCompliance(jsonPointer), json);
    }

    protected abstract T resolveValidPointer(String jsonPointer, T json);

    protected String[] keySegments(String jsonPointer) {
        return Arrays.stream(jsonPointer.split(KEY_SEPARATOR))
            .filter(s -> !s.isEmpty())
            .map(EscapeSequence::unescape)
            .toArray(String[]::new);
    }
}
