package uk.co.paulbenn.jsonpointers;

import java.util.EnumMap;
import java.util.Map;

public enum EscapeSequence {
    SOLIDUS('/', "~1"),
    TILDE('~', "~0");

    // Guarantee declaration order when replacing
    private static final Object PRESENT = new Object();
    private static final Map<EscapeSequence, Object> ESCAPE_SEQUENCES = new EnumMap<>(EscapeSequence.class);
    static {
        for (EscapeSequence escapeSequence: EscapeSequence.values()) {
            ESCAPE_SEQUENCES.put(escapeSequence, PRESENT);
        }
    }

    private final String escapeSequence;
    private final Character character;

    EscapeSequence(Character character, String escapeSequence) {
        this.character = character;
        this.escapeSequence = escapeSequence;
    }

    public Character getCharacter() {
        return character;
    }

    public String getEscapeSequence() {
        return escapeSequence;
    }

    public static String unescape(String escaped) {
        String unescaped = escaped;
        for(EscapeSequence escapeSequence : ESCAPE_SEQUENCES.keySet()) {
            unescaped = unescaped.replace(escapeSequence.getEscapeSequence(), escapeSequence.getCharacter().toString());
        }
        return unescaped;
    }
}
