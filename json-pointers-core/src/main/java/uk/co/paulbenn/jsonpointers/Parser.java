package uk.co.paulbenn.jsonpointers;

@FunctionalInterface
public interface Parser<T> {
    T parse(String jsonPointer);
}
