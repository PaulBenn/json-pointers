package uk.co.paulbenn.jsonpointers;

@FunctionalInterface
public interface ComplianceVerifier {
    String verify(String jsonPointer);
}
