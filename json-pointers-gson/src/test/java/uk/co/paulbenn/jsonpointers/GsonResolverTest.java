package uk.co.paulbenn.jsonpointers;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import uk.co.paulbenn.jsonpointer.gson.GsonResolver;

import static org.junit.jupiter.api.Assertions.assertEquals;

class GsonResolverTest {

    private Resolver<JsonElement> gsonResolver;

    @BeforeEach
    void setUp() {
        gsonResolver = new GsonResolver();
    }

    @Test
    void resolveSimple() {
        JsonObject object = JsonParser.parseString("{ \"abc\" : 123 }").getAsJsonObject();
        JsonElement resolved = gsonResolver.resolve("/abc", object);
        assertEquals(new JsonPrimitive(123), resolved);
    }

    @Test
    void resolveNestedValue() {
        JsonObject object = JsonParser.parseString("{ \"abc\" : { \"def\" : 456 } }").getAsJsonObject();
        JsonElement resolved = gsonResolver.resolve("/abc/def", object);
        assertEquals(new JsonPrimitive(456), resolved);
    }

    @Test
    void resolveHeavilyNestedValue() {
        JsonObject object = JsonParser.parseString(
            "{\"a\":{\"b\":{\"c\":{\"d\":{\"e\":{\"f\":{\"g\":{\"h\":{\"i\":{\"j\":{\"k\":{\"l\":12}}}}}}}}}}}}"
        ).getAsJsonObject();
        JsonElement resolved = gsonResolver.resolve("/a/b/c/d/e/f/g/h/i/j/k/l", object);
        assertEquals(new JsonPrimitive(12), resolved);
    }

    @Test
    void resolveWithinVeryLargeObject() {
        JsonObject object = veryLargeObject();
        JsonElement resolved = gsonResolver.resolve("/key-500/key-500-50/key-500-50-5", object);
        assertEquals(new JsonPrimitive("value-500-50-5"), resolved);
    }

    @Test
    void resolvePointerWithEscapeSequence() {
        JsonObject object = JsonParser.parseString("{ \"/abc~\": 123 }").getAsJsonObject();
        JsonElement resolved = gsonResolver.resolve("/~1abc~0", object);
        assertEquals(new JsonPrimitive(123), resolved);
    }

    @Test
    void resolveArrayIndex() {
        JsonObject object = JsonParser.parseString("{\"abc\": [ 123 ] }").getAsJsonObject();
        JsonElement resolved = gsonResolver.resolve("/abc/0", object);
        assertEquals(new JsonPrimitive(123), resolved);
    }

    @Test
    void resolveArrayIndexWithLeadingZeroes() {
        JsonObject object = JsonParser.parseString("{\"abc\": [ 123, 456 ] }").getAsJsonObject();
        JsonElement resolved = gsonResolver.resolve("/abc/0000001", object);
        assertEquals(new JsonPrimitive(456), resolved);
    }

    private JsonObject veryLargeObject() {
        JsonObject root = new JsonObject();
        for (int i = 0; i < 1000; i++) {
            JsonObject level1Object = new JsonObject();
            for (int j = 0; j < 100; j++) {
                JsonObject level2Object = new JsonObject();
                for (int k = 0; k < 10; k++) {
                    level2Object.add(
                        "key-" + i + "-" + j + "-" + k,
                        new JsonPrimitive("value-" + i + "-" + j + "-" + k)
                    );
                }
                level1Object.add("key-" + i + "-" + j, level2Object);
            }
            root.add("key-" + i, level1Object);
        }
        return root;
    }
}
