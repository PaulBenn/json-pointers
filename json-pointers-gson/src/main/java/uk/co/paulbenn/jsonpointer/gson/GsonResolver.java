package uk.co.paulbenn.jsonpointer.gson;

import com.google.gson.JsonElement;
import uk.co.paulbenn.jsonpointers.Resolver;

public class GsonResolver extends Resolver<JsonElement> {

    @Override
    public JsonElement resolveValidPointer(String jsonPointer, JsonElement json) {
        String[] references = keySegments(jsonPointer);

        JsonElement current = json;

        for (String referenceKey : references) {
            if (referenceKey.chars().allMatch(Character::isDigit)) {
                current = current.getAsJsonArray().get(Integer.parseInt(referenceKey));
            } else {
                current = current.getAsJsonObject().get(referenceKey);
            }
        }

        return current;
    }
}
