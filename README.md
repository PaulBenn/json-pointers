# json-pointers

A small library to resolve JSON pointers against [Jackson][jackson] or [GSON][gson] JSON objects.

Resolved pointers are [RFC6901][rfc6901] compliant. `json-pointers` will throw an exception if it encounters a non-compliant pointer.

## Build
This project uses Gradle. To build on *nix and Windows operating systems:

```bash
gradlew build
```

A rough build process description:
1. Run the [aParse][aParse] parser generator on the RFC6901 Augmented BNF syntax.
2. Beautify and generify generated code using [JavaParser][javaParser] (e.g. add `<T>`).
3. Compile and test `json-pointers-core`.
4. Compile and test dependent modules.

## Usage (Jackson)
Use `json-pointers-jackson`.

```groovy
implementation 'uk.co.paulbenn.jsonpointers:json-pointers-jackson:1.0'
```

Example:
```java
JsonNode object = new ObjectMapper().readTree("{ \"abc\" : 123 }");
new JacksonResolver().resolve("/abc", object) // Resolves to 123
```

## Usage (GSON)
Use `json-pointers-gson`.

```groovy
implementation 'uk.co.paulbenn.jsonpointers:json-pointers-gson:1.0'
```

Example:
```java
JsonObject object = new JsonParser().parse("{ \"abc\" : 123 }").getAsJsonObject();
new GsonResolver().resolve("/abc", object); // Resolves to 123
```

[jackson]: https://github.com/FasterXML/jackson
[gson]: https://github.com/google/gson
[rfc6901]: https://tools.ietf.org/html/rfc6901
[aParse]: https://www.parse2.com/index.shtml
[javaParser]: https://github.com/javaparser/javaparser
