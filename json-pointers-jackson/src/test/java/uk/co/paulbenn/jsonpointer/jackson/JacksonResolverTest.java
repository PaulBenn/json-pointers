package uk.co.paulbenn.jsonpointer.jackson;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.IntNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import uk.co.paulbenn.jsonpointers.Resolver;

import static org.junit.jupiter.api.Assertions.assertEquals;

class JacksonResolverTest {

    private ObjectMapper objectMapper;
    
    private Resolver<JsonNode> jacksonResolver;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
        jacksonResolver = new JacksonResolver();
    }

    @Test
    void resolveSimple() throws JsonProcessingException {
        JsonNode object = objectMapper.readTree("{ \"abc\" : 123 }");
        JsonNode resolved = jacksonResolver.resolve("/abc", object);
        assertEquals(new IntNode(123), resolved);
    }

    @Test
    void resolveNestedValue() throws JsonProcessingException {
        JsonNode object = objectMapper.readTree("{ \"abc\" : { \"def\" : 456 } }");
        JsonNode resolved = jacksonResolver.resolve("/abc/def", object);
        assertEquals(new IntNode(456), resolved);
    }

    @Test
    void resolveHeavilyNestedValue() throws JsonProcessingException {
        JsonNode object = objectMapper.readTree(
            "{\"a\":{\"b\":{\"c\":{\"d\":{\"e\":{\"f\":{\"g\":{\"h\":{\"i\":{\"j\":{\"k\":{\"l\":12}}}}}}}}}}}}"
        );
        JsonNode resolved = jacksonResolver.resolve("/a/b/c/d/e/f/g/h/i/j/k/l", object);
        assertEquals(new IntNode(12), resolved);
    }

    @Test
    void resolveWithinVeryLargeObject() {
        JsonNode object = veryLargeObject();
        JsonNode resolved = jacksonResolver.resolve("/key-500/key-500-50/key-500-50-5", object);
        assertEquals(new TextNode("value-500-50-5"), resolved);
    }

    @Test
    void resolvePointerWithEscapeSequence() throws JsonProcessingException {
        JsonNode object = objectMapper.readTree("{ \"/abc~\": 123 }");
        JsonNode resolved = jacksonResolver.resolve("/~1abc~0", object);
        assertEquals(new IntNode(123), resolved);
    }

    @Test
    void resolveArrayIndex() throws JsonProcessingException {
        JsonNode object = objectMapper.readTree("{\"abc\": [ 123 ] }");
        JsonNode resolved = jacksonResolver.resolve("/abc/0", object);
        assertEquals(new IntNode(123), resolved);
    }

    @Test
    void resolveArrayIndexWithLeadingZeroes() throws JsonProcessingException {
        JsonNode object = objectMapper.readTree("{\"abc\": [ 123, 456 ] }");
        JsonNode resolved = jacksonResolver.resolve("/abc/0000001", object);
        assertEquals(new IntNode(456), resolved);
    }

    private JsonNode veryLargeObject() {
        ObjectNode root = objectMapper.getNodeFactory().objectNode();
        for (int i = 0; i < 1000; i++) {
            ObjectNode level1Object = objectMapper.getNodeFactory().objectNode();
            for (int j = 0; j < 100; j++) {
                ObjectNode level2Object = objectMapper.getNodeFactory().objectNode();
                for (int k = 0; k < 10; k++) {
                    level2Object.set(
                        "key-" + i + "-" + j + "-" + k,
                        new TextNode("value-" + i + "-" + j + "-" + k)
                    );
                }
                level1Object.set("key-" + i + "-" + j, level2Object);
            }
            root.set("key-" + i, level1Object);
        }
        return root;
    }
}
