package uk.co.paulbenn.jsonpointer.jackson;

import com.fasterxml.jackson.databind.JsonNode;
import uk.co.paulbenn.jsonpointers.Resolver;

public class JacksonResolver extends Resolver<JsonNode> {

    @Override
    public JsonNode resolveValidPointer(String jsonPointer, JsonNode json) {
        String[] references = keySegments(jsonPointer);

        JsonNode current = json;

        for (String referenceKey : references) {
            if (referenceKey.chars().allMatch(Character::isDigit)) {
                current = current.get(Integer.parseInt(referenceKey));
            } else {
                current = current.get(referenceKey);
            }
        }

        return current;
    }
}
