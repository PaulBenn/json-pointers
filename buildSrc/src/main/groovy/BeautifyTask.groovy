import beautify.BeautifyTaskRunnable
import org.gradle.api.DefaultTask
import org.gradle.api.tasks.TaskAction

class BeautifyTask extends DefaultTask {

    @TaskAction
    void beautify() {
        new BeautifyTaskRunnable(project).run()
    }
}
