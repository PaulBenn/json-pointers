package beautify;

import com.github.javaparser.ParseResult;
import com.github.javaparser.ast.CompilationUnit;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.utils.SourceRoot;
import org.gradle.api.Project;
import org.gradle.api.logging.Logger;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class BeautifyTaskRunnable implements Runnable {

    private final Project project;

    private final Path genSrcPath;

    private final ModifierVisitor<Logger> beautifierVisitor;

    public BeautifyTaskRunnable(Project project) {
        this.project = project;
        this.genSrcPath = project.getProjectDir().toPath().resolve(Paths.get("src", "gen", "java"));
        this.beautifierVisitor = new BeautifierVisitor();
    }

    @Override
    public void run() {
        SourceRoot genSrcRoot = new SourceRoot(genSrcPath);

        project.getLogger().info("generated source root: {}", genSrcPath.toString());

        List<ParseResult<CompilationUnit>> parseResults = genSrcRoot.tryToParseParallelized();

        for (ParseResult<CompilationUnit> parseResult : parseResults) {
            if (parseResult.isSuccessful()) {
                CompilationUnit compilationUnit = parseResult.getResult().orElseThrow();
                project.getLogger().info("beautifying " + compilationUnit.getPrimaryTypeName());
                compilationUnit.accept(beautifierVisitor, project.getLogger());
                write(compilationUnit);
            } else {
                project.getLogger().error(parseResult.toString());
            }
        }
    }

    public void write(CompilationUnit compilationUnit) {
        compilationUnit.getStorage().ifPresentOrElse(
            s -> write(compilationUnit, s),
            () -> {
                throw new IllegalStateException(
                    "test class '" + compilationUnit.getPrimaryTypeName() + "' was not loaded from a file"
                );
            }
        );
    }

    private void write(CompilationUnit compilationUnit, CompilationUnit.Storage storage) {
        Path newPath = genSrcPath.resolve(storage.getSourceRoot().relativize(storage.getPath()));
        try {
            Files.writeString(newPath, compilationUnit.toString());
        } catch (IOException e) {
            project.getLogger().error("could not write test class {}", storage.getPath(), e);
            throw new UncheckedIOException(e);
        }
    }
}
