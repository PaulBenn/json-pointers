package beautify;


import com.github.javaparser.StaticJavaParser;
import com.github.javaparser.ast.Modifier;
import com.github.javaparser.ast.NodeList;
import com.github.javaparser.ast.body.ClassOrInterfaceDeclaration;
import com.github.javaparser.ast.body.FieldDeclaration;
import com.github.javaparser.ast.body.MethodDeclaration;
import com.github.javaparser.ast.body.VariableDeclarator;
import com.github.javaparser.ast.expr.AnnotationExpr;
import com.github.javaparser.ast.expr.SingleMemberAnnotationExpr;
import com.github.javaparser.ast.expr.StringLiteralExpr;
import com.github.javaparser.ast.type.TypeParameter;
import com.github.javaparser.ast.visitor.ModifierVisitor;
import com.github.javaparser.ast.visitor.Visitable;
import org.gradle.api.logging.Logger;

import java.util.Arrays;
import java.util.stream.Collectors;

public class BeautifierVisitor extends ModifierVisitor<Logger> {

    private static final TypeParameter TYPE_PARAMETER = new TypeParameter("T");

    @Override
    public Visitable visit(ClassOrInterfaceDeclaration node, Logger logger) {
        String className = node.getNameAsString();
        if ("Rule".equals(className)) {
            logger.info("  [{}] generifying 'accept' method", className);
            generifyAcceptMethod(node);
        } else if (className.startsWith("Rule_") || className.startsWith("Terminal_")) {
            logger.info("  [{}] adding 'RULE_ID' field", className);
            addRuleIdField(node);
            logger.info("  [{}] generifying 'accept' method", className);
            generifyAcceptMethod(node);
        } else if ("Visitor".equals(className)) {
            logger.info("  [{}] generifying interface declaration", className);
            generifyInterfaceDeclaration(node);
            logger.info("  [{}] generifying 'visit' methods", className);
            generifyVisitMethods(node);
        } else if (className.endsWith("Displayer")) {
            logger.info("  [{}] generifying class declaration", className);
            addDefaultTypeParameter(node);
        } else if ("Parser".equals(className)) {
            logger.info("  [{}] adding @SuppressWarnings(\"unchecked\")", className);
            suppressUncheckedWarnings(node);
        } else if ("ParserException".equals(className)) {
            logger.info("  [{}] changing to unchecked exception", className);
            makeUnchecked(node);
        }
        return super.visit(node, logger);
    }

    private void addRuleIdField(ClassOrInterfaceDeclaration node) {
        FieldDeclaration fieldDeclaration = node.addField(
            String.class,
            "RULE_ID",
            Modifier.Keyword.PUBLIC,
            Modifier.Keyword.STATIC,
            Modifier.Keyword.FINAL
        );
        VariableDeclarator declarator = fieldDeclaration.getVariables().getFirst().orElseThrow();
        declarator.setInitializer(new StringLiteralExpr(deriveAbnfRuleName(node.getName().asString())));
    }

    private String deriveAbnfRuleName(String generatedClassName) {
        String[] parts = generatedClassName.split("_");
        return Arrays
            .stream(parts, 1, parts.length)
            .collect(Collectors.joining("-"));
    }

    private void generifyAcceptMethod(ClassOrInterfaceDeclaration node) {
        MethodDeclaration acceptMethod = node.getMethodsByName("accept").get(0);
        acceptMethod.setType(TYPE_PARAMETER);
        acceptMethod.setTypeParameters(NodeList.nodeList(TYPE_PARAMETER));
        acceptMethod
            .getParameterByName("visitor")
            .orElseThrow()
            .getType()
            .asClassOrInterfaceType()
            .setTypeArguments(NodeList.nodeList(TYPE_PARAMETER));
    }

    private void generifyInterfaceDeclaration(ClassOrInterfaceDeclaration node) {
        node.setTypeParameters(NodeList.nodeList(TYPE_PARAMETER));
    }

    private void generifyVisitMethods(ClassOrInterfaceDeclaration node) {
        node.getMethods()
            .stream()
            .filter(m -> "visit".equals(m.getNameAsString()))
            .forEach(m -> m.setType(TYPE_PARAMETER));
    }

    private void addDefaultTypeParameter(ClassOrInterfaceDeclaration node) {
        TypeParameter objectType = new TypeParameter(Object.class.getSimpleName());
        node
            .getImplementedTypes()
            .getFirst()
            .orElseThrow()
            .setTypeArguments(NodeList.nodeList(objectType));
    }

    private void suppressUncheckedWarnings(ClassOrInterfaceDeclaration node) {
        AnnotationExpr suppressWarnings = new SingleMemberAnnotationExpr(
            StaticJavaParser.parseName(SuppressWarnings.class.getSimpleName()),
            new StringLiteralExpr("unchecked")
        );
        node.addAnnotation(suppressWarnings);
    }

    private void makeUnchecked(ClassOrInterfaceDeclaration node) {
        node
            .getExtendedTypes()
            .getFirst()
            .orElseThrow()
            .setName(RuntimeException.class.getSimpleName());
    }
}
